CFLAGS=--std=c11 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
CC=gcc

all: img_rotation
util.o: util.c
	$(CC) -c $(CFLAGS) $< -o $@

image.o: image.c
	$(CC) -c $(CFLAGS) $< -o $@

rotate.o: rotate.c
	$(CC) -c $(CFLAGS) $< -o $@

bmp.o: bmp.c
	$(CC) -c $(CFLAGS) $< -o $@

ocfile.o: ocfile.c
	$(CC) -c $(CFLAGS) $< -o $@

main.o: main.c
	$(CC) -c $(CFLAGS) $< -o $@

img_rotation: util.o image.o rotate.o bmp.o ocfile.o main.o
	$(CC) -o img_rotation $^

clean:
	rm -f util.o image.o rotate.o bmp.o ocfile.o main.o img_rotation
