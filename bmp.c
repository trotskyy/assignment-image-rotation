//
// Created by Home on 22.01.2021.
//

#include "bmp.h"
#include "util.h"
#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>

#define MB 19778 //bfType

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

static struct bmp_header new_header(uint32_t width, uint32_t height) {
    return (struct bmp_header) {
        .bfType =MB,
        .bfileSize = sizeof(struct bmp_header) + width * height * sizeof( struct pixel ),
        .bfReserved = 0,
        .bOffBits = sizeof( struct bmp_header ),
        .biSize = 40,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = 0,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };

}

static enum read_status read_header(FILE* file, struct bmp_header* header) {
    if (!fread ( header, sizeof ( struct  bmp_header), 1, file)) return READ_INVALID_HEADER;
    if ( header->bfType != MB ) return READ_INVALID_SIGNATURE;
    if ( header->biBitCount != 24 ) return READ_INVALID_BITS;
    return READ_OK;
}

uint32_t get_padding(struct image const * image) {
    return (4 - (image->width * 3 % 4)) % 4;
}

enum read_status from_bmp( FILE* in, struct image* image ) {
    struct bmp_header  header = { 0 };
    const enum read_status status = read_header(in, &header);
    if (status) {
        free_memory(image);
        return status;
    }
    if (fseek( in, sizeof(struct bmp_header), SEEK_SET)){
        free_memory(image);
        return READ_ERROR;
    }
    const uint32_t width = header.biWidth;
    const uint32_t height = header.biHeight;
    image->width = width;
    image->height = height;
    image->data = (struct pixel *) malloc(width * height * sizeof(struct pixel));
    const uint32_t padding = get_padding( image );
    for (size_t i = 0; i < image->height; i++) {
        if (fread( &(image->data[i * width] ), sizeof (struct pixel), width, in) != width) {
            free_memory(image);
            return READ_ERROR;
        }
        if (fseek(in, padding, SEEK_CUR)) {
            free_memory(image);
            return READ_ERROR;
        }
    }
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* image ) {
    const uint32_t width = image->width;
    const uint32_t height = image->height;
    const uint32_t padding = get_padding( image );
    const struct bmp_header header = new_header( width, height );
    const uint8_t empty[] = {0, 0, 0};
    if (!fwrite(&header, sizeof( struct bmp_header ), 1, out )) return WRITE_ERROR;
    for (size_t i = 0; i < height; i++) {
        if (fwrite( &( image->data[i * width] ), sizeof( struct pixel ), width, out ) != width) return WRITE_ERROR;
        if (fwrite(empty, 1, padding, out) != padding) return WRITE_ERROR;
    }
    return WRITE_OK;
}
