//
// Created by Home on 22.01.2021.
//

#ifndef IMG_ROTATION_BMP_H
#define IMG_ROTATION_BMP_H

#include "image.h"
#include "util.h"
#include <stdio.h>

enum read_status from_bmp( FILE* in, struct image* img );

enum write_status to_bmp( FILE* out, struct image const* img );

#endif //IMG_ROTATION_BMP_H
