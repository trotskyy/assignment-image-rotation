//
// Created by Home on 22.01.2021.
//

#ifndef IMG_ROTATION_IMAGE_H
#define IMG_ROTATION_IMAGE_H

#include <stdint.h>
#include <stdlib.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint32_t width, height;
    struct pixel* data;
};

void free_memory(struct image * image);

#endif //IMG_ROTATION_IMAGE_H
