#include <stdio.h>
#include <stdbool.h>

#include "ocfile.h"
#include "bmp.h"
#include "rotate.h"
#include "util.h"

void usage() {
    fprintf(stderr, "Usage: img_rotation BMP_FILE_NAME\n");
}

int main(int argc, char** argv) {
    if (argc != 2) usage();
    if (argc < 2) err("Not enough arguments \n" );
    if (argc > 2) err("Too many arguments \n" );

    FILE * image_file = NULL;
    enum file_opening_status file_opening_status = open_read_file(&image_file, argv[1]);
    if (file_opening_status) err(get_file_opening_status_message(file_opening_status));
    msg(get_file_opening_status_message(file_opening_status));

    struct image img;
    enum read_status read_status = from_bmp(image_file, &img);
    if (read_status) err(get_read_status_message(read_status));
    msg(get_read_status_message(read_status));

    enum file_closing_status file_closing_status = close_file(image_file);
    if (file_closing_status) err(get_file_closing_status_message(file_closing_status));
    msg(get_file_closing_status_message(file_closing_status));

    struct image rotated_image = rotate_image(img);
    msg("Rotated image ready");

    file_opening_status = open_write_file(&image_file, argv[1]);
    if (file_opening_status) err(get_file_opening_status_message(file_opening_status));
    msg(get_file_opening_status_message(file_opening_status));

    enum write_status write_status = to_bmp(image_file, &rotated_image);
    if (write_status) err(get_write_status_message(write_status));

    msg(get_write_status_message(write_status));

    free_memory(&rotated_image);

    file_closing_status = close_file(image_file);
    if (file_closing_status) err(get_file_closing_status_message(file_closing_status));
    msg(get_file_closing_status_message(file_closing_status));

    return 0;

}
