//
// Created by Home on 22.01.2021.
//

#include "ocfile.h"

enum file_opening_status open_read_file(FILE** file, char* name) {
    if (!name) return FILE_INVALID_PATH;
    *file = fopen(name, "rb");
    if (!(*file)) return INVALID_FILE;
    return FILE_READ_OK;
}

enum file_opening_status open_write_file(FILE** file, char* name) {
    if (!name) return FILE_INVALID_PATH;
    *file = fopen(name, "wb");
    if (!(*file)) return INVALID_FILE;
    return FILE_READ_OK;
}

enum file_closing_status close_file(FILE *file) {
    if (fclose(file)) return FILE_WRITE_ERROR;
    return FILE_WRITE_OK;
}
