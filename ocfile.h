//
// Created by Home on 22.01.2021.
//

#ifndef IMG_ROTATION_OCFILE_H
#define IMG_ROTATION_OCFILE_H

#include "util.h"

#include <stdio.h>
enum file_opening_status open_read_file(FILE** file, char* name);
enum file_opening_status open_write_file(FILE** file, char* name);
enum file_closing_status close_file(FILE* file);

#endif //IMG_ROTATION_OCFILE_H
