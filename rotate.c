//
// Created by Home on 25.01.2021.
//

#include "rotate.h"

struct image rotate_image(struct image image) {
    struct image new_image = {
        .width = image.height,
        .height = image.width,
            .data = (struct pixel *) malloc(image.height * image.width * sizeof (struct pixel))
    };
    size_t new_col, new_row = 0;
    for (size_t i = 0; i < image.width; i++) {
        new_col = 0;
        for (size_t j = image.height; j > 0; j--) {
            new_image.data[new_image.width * new_row + new_col] = image.data[image.width * (j - 1) + i];
            new_col++;
        }
        new_row++;
    }
    free_memory(&image);
    return new_image;
}
