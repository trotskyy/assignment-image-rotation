//
// Created by Home on 25.01.2021.
//

#ifndef IMG_ROTATION_ROTATE_H
#define IMG_ROTATION_ROTATE_H

#include "image.h"

struct image rotate_image(struct image image);

#endif //IMG_ROTATION_ROTATE_H
