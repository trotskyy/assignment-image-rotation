//
// Created by Home on 25.01.2021.
//

#include "util.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

_Noreturn void err( const char* msg, ...) {
    va_list args;
    va_start ( args, msg );
    vfprintf( stderr, msg, args );
    va_end( args );
    exit( 1 );
}

void msg( const char* msg, ...) {
    va_list args;
    va_start ( args, msg );
    vfprintf( stdout, msg, args );
    fprintf( stdout, "\n");
    va_end( args );
}


static const char* const read_status_messages[] = {
        "Image reading finished",
        "Invalid signature",
        "Invalid bits",
        "Invalid header",
        "Pixels reading error"
};

static const char* const write_status_messages[] = {
        "Image writing finished",
        "Writing error"
};

static const char* const file_opening_status_messages[] = {
        "File opened",
        "Invalid path to file",
        "Invalid file"
};

static const char* const file_closing_status_messages[] = {
        "File closed",
        "File closing error"
};

static const char* const undefined_error = "Undefined error happened";

const char * get_read_status_message (enum read_status status) {
    if (status >= 0 && status < 5) return read_status_messages[status];
    return undefined_error;
}
const char * get_write_status_message (enum write_status status) {
    if (status >= 0 && status < 2) return write_status_messages[status];
    return undefined_error;
}
const char * get_file_opening_status_message (enum file_opening_status status) {
    if (status >= 0 && status < 3) return file_opening_status_messages[status];
    return undefined_error;
}
const char * get_file_closing_status_message (enum file_closing_status status) {
    if (status >= 0 && status < 2) return file_closing_status_messages[status];
    return undefined_error;
}