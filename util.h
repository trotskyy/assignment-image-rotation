//
// Created by Home on 25.01.2021.
//

#ifndef IMG_ROTATION_UTIL_H
#define IMG_ROTATION_UTIL_H

_Noreturn void err( const char* msg, ... );

void msg( const char* msg, ...);

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum file_opening_status {
    FILE_READ_OK = 0,
    FILE_INVALID_PATH,
    INVALID_FILE
};

enum file_closing_status {
    FILE_WRITE_OK = 0,
    FILE_WRITE_ERROR
};

const char * get_read_status_message (enum read_status status);
const char * get_write_status_message (enum write_status status);
const char * get_file_opening_status_message (enum file_opening_status status);
const char * get_file_closing_status_message (enum file_closing_status status);

#endif //IMG_ROTATION_UTIL_H
